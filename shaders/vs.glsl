uniform float x_off;
uniform float y_off;
out vec2 v_uv;

const vec2[4] QUAD_POS = vec2[](
    vec2(-0.16, -0.16),
    vec2( 0.16, -0.16),
    vec2( 0.16,  0.16),
    vec2(-0.16,  0.16)
);

void main() {
    vec2 p = QUAD_POS[gl_VertexID];

    gl_Position = vec4(p.x + x_off, p.y + y_off, 0., 1.);
    v_uv = p * 6.25;
}