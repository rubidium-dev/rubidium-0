extern crate image;
#[macro_use]
extern crate luminance;
extern crate luminance_glfw;

use luminance::blending::{Equation, Factor};
use luminance::context::GraphicsContext;
use luminance::depth_test::DepthTest;
use luminance::framebuffer::Framebuffer;
use luminance::pipeline::BoundTexture;
use luminance::pixel::RGBA32F;
use luminance::render_state::RenderState;
use luminance::shader::program::Program;
use luminance::tess::{Mode, Tess};
use luminance::texture::{Dim2, Flat, Sampler, Texture};
use luminance_glfw::event::{Action, Key, WindowEvent};
use luminance_glfw::surface::{GlfwSurface, Surface, WindowDim, WindowOpt};
use std::path::Path;

const VS: &'static str = include_str!("../shaders/vs.glsl");
const FS: &'static str = include_str!("../shaders/fs.glsl");

fn main() {
    run(Path::new("assets/char.png"));
}

uniform_interface! {
    struct ShaderInterface {
        x_off: f32,
        y_off: f32,
        tex: &'static BoundTexture<'static, Flat, Dim2, RGBA32F>
    }
}

fn run(texture_path: &Path) {
    let mut surface = GlfwSurface::new(
        WindowDim::Windowed(400, 400),
        "rubidium-0",
        WindowOpt::default(),
    )
    .expect("GLFW surface creation");

    let tex = load_from_disk(&mut surface, Path::new(&texture_path)).expect("texture loading");

    let (program, _) = Program::<(), (), ShaderInterface>::from_strings(None, VS, None, FS)
        .expect("program creation");
    let tess = Tess::attributeless(&mut surface, Mode::TriangleFan, 4);
    let mut back_buffer = Framebuffer::back_buffer(surface.size());
    let mut x_off: f32 = 0.;
    let mut y_off: f32 = 0.;

    println!("rendering!");

    'app: loop {
        for event in surface.poll_events() {
            match event {
                WindowEvent::Close | WindowEvent::Key(Key::Escape, _, Action::Release, _) => {
                    break 'app
                }

                WindowEvent::Key(Key::Left, _, Action::Press, _)
                | WindowEvent::Key(Key::Left, _, Action::Repeat, _) => {
                    x_off = (x_off - 0.04).max(-1.);
                }

                WindowEvent::Key(Key::Right, _, Action::Press, _)
                | WindowEvent::Key(Key::Right, _, Action::Repeat, _) => {
                    x_off = (x_off + 0.04).min(0.84);
                }

                WindowEvent::Key(Key::Down, _, Action::Press, _)
                | WindowEvent::Key(Key::Down, _, Action::Repeat, _) => {
                    y_off = (y_off - 0.04).max(-1.);
                }

                WindowEvent::Key(Key::Up, _, Action::Press, _)
                | WindowEvent::Key(Key::Up, _, Action::Repeat, _) => {
                    y_off = (y_off + 0.04).min(0.84);
                }

                WindowEvent::FramebufferSize(width, height) => {
                    back_buffer = Framebuffer::back_buffer([width as u32, height as u32]);
                }

                _ => (),
            }
        }

        surface.pipeline_builder().pipeline(
            &back_buffer,
            [0., 0., 0.4, 1.],
            |pipeline, shd_gate| {
                let bound_tex = pipeline.bind_texture(&tex);

                shd_gate.shade(&program, |rdr_gate, iface| {
                    iface.tex.update(&bound_tex);
                    iface.x_off.update(x_off);
                    iface.y_off.update(y_off);

                    let state = RenderState::default()
                        .set_depth_test(DepthTest::Disabled)
                        .set_blending((
                            Equation::Additive,
                            Factor::SrcAlpha,
                            Factor::SrcAlphaComplement,
                        ));
                    rdr_gate.render(state, |tess_gate| {
                        tess_gate.render(&mut surface, (&tess).into());
                    });
                });
            },
        );

        surface.swap_buffers();
    }
}

fn load_from_disk(surface: &mut GlfwSurface, path: &Path) -> Option<Texture<Flat, Dim2, RGBA32F>> {
    match image::open(&path) {
        Ok(img) => {
            let rgb_img = img.flipv().to_rgba();
            let (width, height) = rgb_img.dimensions();
            let texels = rgb_img
                .pixels()
                .map(|rgb| {
                    (
                        rgb[0] as f32 / 255.,
                        rgb[1] as f32 / 255.,
                        rgb[2] as f32 / 255.,
                        rgb[3] as f32 / 255.,
                    )
                })
                .collect::<Vec<_>>();

            let mut sampler = Sampler::default();
            sampler.mag_filter = luminance::texture::MagFilter::Nearest;
            let tex = Texture::new(surface, [width, height], 0, &sampler)
                .expect("luminance texture creation");
            tex.upload(false, &texels);

            Some(tex)
        }

        Err(e) => {
            eprintln!("cannot open image {}: {}", path.display(), e);
            None
        }
    }
}
